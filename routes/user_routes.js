const express = require('express');
const UserController = require ('../controllers/UserController')

let router = express.Router();

router.route('/users').get(UserController.index).post(UserController.create);

router.route('/users/:id').get(UserController.show).put(UserController.update).delete(UserController.destroy);

module.exports = router;
