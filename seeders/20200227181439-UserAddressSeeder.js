const faker = require('faker');
const {Op} = require('sequelize');
const User = require('../models').User;

'use strict';


module.exports = {
  up: (queryInterface, Sequelize) => {

    var fakeUsersAddresses = [{
      user_id: 1,
      state: 'Distrito Federal',
      country: 'Venezuela'
    }];

      return User.findAll({
        where: {
          id: {
            [Op.ne]:1
          }
        }
      }).then((users)=>{
        users.forEach(user => {
          fakeUsersAddresses.push({
            user_id: user.dataValues.id,
            state: faker.address.state(),
            country: faker.address.country()
          });
          //console.log(fakeUsersAddresses);
        });
        queryInterface.bulkInsert('user_addresses', fakeUsersAddresses, {});
      });

  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('user_addresses', null, {});
  }
};
