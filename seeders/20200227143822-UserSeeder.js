const faker = require('faker');

'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    var fakeUsers = [{
      firstName: 'Kadievka',
      lastName: 'Salcedo',
      email: 'kadievka@mail.com'
    }];

    for ( i=0; i<99 ; i++ ){
      fakeUsers.push({
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
      });
    }

    //console.log(fakeUsers);

      return queryInterface.bulkInsert('users', fakeUsers, {});
  },

  down: (queryInterface, Sequelize) => {
    
      return queryInterface.bulkDelete('users', null, {});
  }
};
