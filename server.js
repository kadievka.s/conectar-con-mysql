//IMPORT LIBS
const express = require ('express');

//IMPORT ROUTES
const userRoutes = require('./routes/user_routes');

//CONECT
const app = express();

//USE ROUTES
app.use(userRoutes);

//HOME
app.get('/', function(req,res){

    res.send('hello world, this is HOMEPAGE').status(200);

});

app.listen(3000);