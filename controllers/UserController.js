const User = require('../models').User
const User_Address = require('../models').User_Address

module.exports = {
    index: function(req, res){

        User.findAll({
            attributes: ['id', 'firstName', 'lastName', 'email'],
            include: {
                model: User_Address,
                as: 'user_addresses',
                attributes: ['id', 'state', 'country']
            }
        }).then((user)=>{
            res.json(user).status(200);
            console.log(user[0].dataValues.id);
        }).catch(err=>{
            res.json(err);
            console.log(err);
        });
    },

    show: function(req, res){
        
        User.findByPk(req.params.id).then((user)=>{
            res.json(user);
        }).catch(err=>{
            res.json(err);
            console.log(err);
        });


    }
};
