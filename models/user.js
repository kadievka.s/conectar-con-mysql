'use strict';

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING
  }, {
    tableName: 'users'
  });
  User.associate = function(models) {
    User.hasMany(models.User_Address, {
      as: 'user_addresses', 
      foreignKey: {
        field: 'user_id'
      },
      constraints: true
    });
  };
  return User;
};