'use strict';

module.exports = (sequelize, DataTypes) => {
  const User_Address = sequelize.define('User_Address', {
      user_id: {
        type: DataTypes.INTEGER,
        references:{
          model:{
            tableName: 'users'
          },
          key: 'id'
        }
      },
      state: DataTypes.STRING,
      country: DataTypes.STRING
  }, {
    tableName: 'user_addresses'
  });
  User_Address.associate = function(models) {
    User_Address.belongsTo(models.User);
  };
  return User_Address;
};